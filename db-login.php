<?php
$db = mysqli_connect('localhost', 'root', '', 'php_api');
if (!$db) {
    echo json_encode(array("status" => "Error", "message" => "Database Connection Failed"));
    exit;
}

$email = $_POST["email"];
$password = $_POST["password"];

$stmt = $db->prepare("SELECT * FROM users WHERE email = ?");
$stmt->bind_param("s", $email);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows == 1) {
    $user = $result->fetch_assoc();
    $hashedPassword = $user['password'];
    if (password_verify($password, $hashedPassword)) {
        header('Location: index.html');
            exit();
        echo json_encode(array("status" => "Success", "message" => "Login Success"));
    } else {
        echo json_encode(array("status" => "Error", "message" => "Incorrect Password"));
    }
} else {
    echo json_encode(array("status" => "Error", "message" => "User Not Found"));
}

mysqli_close($db);
?>
