<?php
$db = mysqli_connect('localhost', 'root', '', 'php_api');
if (!$db) {
    echo json_encode(array("status" => "Error", "message" => "Database Connection Failed"));
    exit;
}

$username = $_POST["username"];
$email = $_POST["email"];
$password = $_POST["password"];

if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
    echo json_encode(array("status" => "Error", "message" => "Invalid Email"));
    exit;
}

$hashedPassword = password_hash($password, PASSWORD_BCRYPT);

$stmt = $db->prepare("SELECT * FROM users WHERE username = ?");
$stmt->bind_param("s", $username);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows == 1) {
    echo json_encode(array("status"=> "Error", "message" => "User Already Exists"));
} else {
    $stmt = $db->prepare("INSERT INTO users (username, email, password) VALUES (?, ?, ?)");
    $stmt->bind_param("sss", $username, $email, $hashedPassword); // Use $hashedPassword instead of $password
    if ($stmt->execute()) {
        echo json_encode(array("status" => "Success", "message" => "Registration Success"));
        header('Location: login.html');
        exit();
    } else {
        echo json_encode(array("status" => "Error", "message" => "Registration Failed"));
    }
}

mysqli_close($db);
?>
